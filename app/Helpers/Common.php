<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class Common
{
    public static function generateDeck()
    {
        $suits = ['S', 'H', 'D', 'C'];
        $values = array_merge(range(2, 9), ['A', 'X', 'J', 'Q', 'K']);
        $deck = [];

        foreach ($suits as $suit) {
            foreach ($values as $value) {
                $deck[] = $suit . '-' . $value;
            }
        }
        shuffle($deck);
        return $deck;
    }

    public static function splitCards($no_players = 1, $deck = null)
    {
        if (!$deck) {
            $deck = self::generateDeck();
        }
        $players = array_fill(0, $no_players, []);


        foreach ($deck as $i => $card) {
            $playerIndex = $i % $no_players;
            $players[$playerIndex][] = $card;
        }

        return $players;
    }
}
