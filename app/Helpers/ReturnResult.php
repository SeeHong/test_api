<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class ReturnResult
{
    public $isSuccess = true;
    public $code = "";
    public $status = 200;
    public $message = "";
    public $data = [];
    public $filter = "";
    public $locale = [];

    public function setError500()
    {
        $this->isSuccess = false;
        $this->status = 500;
        $this->code = 'INTERNAL_SERVER_ERROR';
        $this->message = "Something Went Wrong. Please Contact Administrator";
    }

    //missing bad request
    public function setError400($code, $attribute = [])
    {
        $this->isSuccess = false;
        $this->status = 400;
        $this->message = "Bad Request";
    }

    //missing bad request
    public function setError404($message = "")
    {
        $this->isSuccess = false;
        $this->status = 404;
        $this->message = $message;
    }

    //user invalid
    public function setError401($code, $attribute = [])
    {
        $this->isSuccess = false;
        $this->status = 400;
        $this->code = $code;
        $this->message = "Invalid User";
    }

    //forbiden
    public function setError403($message, $code = "VALIDATION_FAILED")
    {
        $this->isSuccess = false;
        $this->status = 403;
        $this->code = $code;
        $this->message = $message;
    }

    //exists
    public function setError409($code)
    {
        $this->isSuccess = false;
        $this->status = 409;
        $this->code = $code;
        $this->message = "Data exists";
    }

    public function setSessionTimeout()
    {
        $this->isSuccess = false;
        $this->status = 401;
        $this->code = "AUTH_REQUIRED";
        $this->message = "Session Timeout";
    }

    public function setError($code = null, $status = 500)
    {
        $this->isSuccess = false;
        $this->code = $code;
        $this->status = $status;
        $this->message = "Internal Server Error";
    }
}
