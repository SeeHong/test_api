<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ReturnResult;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Common;
use Carbon\Carbon;

class CardController extends Controller
{
    public function startGame(Request $request)
    {
        $result = new ReturnResult();
        $validator = Validator::make($request->all(), [
            'number_of_people' => 'Numeric|Required',
        ]);
        if ($validator->fails()) {
            $result->setError403($validator->errors()->first());
            return response()->json($result);
        }
        try {
            $deck = Common::generateDeck();
            $players_with_cards = Common::splitCards($request->number_of_people);

            # contract data format
            $data = [];
            foreach($players_with_cards as $player) {
                $cards = implode(', ', $player);
                $data[] = $cards;
            }
            $result->data = $data;
            return response()->json($result);
        } catch (\Exception $e) {
            Log::error($e);
            $result->setError500();
        }
        return response()->json($result);
    }
}
